# User Manager (Angular7 Front-end interface) 

This the front end interface built with [Angular](https://angular.io/).

## About

This site uses a Laravel backend as api for authentication and CRUD operations of users.

### Prerequisites

To run this you need to have [Angular CLI](https://cli.angular.io/).

```
npm install -g @angular/cli
```

### Installing

Follow these steps to run and build the app.

Navigate to the project folder and run following command.

```
npm install
```

After this you need to go to  src/app/app-settings.ts and update API_ENDPOINT variable to your laravel api url.

```
public static API_ENDPOINT = '<YOUR API URL>';
```

And then to run the app locally,

```
npm start
```

The admin credentials to login into this application are:
```
User     : admin@demo.com
Password : Password123
```

## Deployment

To build the app for production, run following command from inside the project.

```
ng build --prod
```


## Built With

* [Angular](https://angular.io/) - The web framework used
* [Bootstrap](https://getbootstrap.com/) - Styling
* [ng-Bootstrap](https://ng-bootstrap.github.io/#/home) - Styling


## Author

* **Shahzan M Siddique** - 


