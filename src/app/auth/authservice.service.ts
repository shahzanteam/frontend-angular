import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app-settings';

@Injectable({
  providedIn: 'root'
})
/* This service handles http requests sent back-end for 
authentication. */
export class AuthserviceService {

  constructor(private http: HttpClient) { }

  signIn(formData) {
    return this.http.post(AppSettings.API_ENDPOINT + 'login', formData);
  }

}
