import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthserviceService } from '../authservice.service';
import { TokenService } from '../token.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
  public form = {
    email: "", password: ""
  }
  private authResponse: any;
  public error = null;
  constructor(
    private router: Router,
    private authService: AuthserviceService,
    private token: TokenService) {
  }
  onSubmit() {
    this.authResponse = this.authService.signIn(this.form).subscribe(
      data => { this.handleResponse(data); },
      error => {
        this.error = error.error.error;
      }
    );
  }

  handleResponse(data: any) {
    this.token.handleToken(data.access_token);
    this.router.navigate(['/home/users']);
  }
}
