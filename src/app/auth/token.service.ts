import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings } from '../app-settings';

@Injectable({
  providedIn: 'root'
})

/* This service handles the auth token recieved from backend ,
  and provides function to check if user is authenticated. */
export class TokenService {

  constructor(private router: Router) { }

  handleToken(token) {
    this.set(token);
  }

  set(token) {
    localStorage.setItem('token', token);
  }

  get() {
    return localStorage.getItem('token');
  }
  remove() {
    localStorage.removeItem('token');
    return 1;
  }

  /* verifies the token */
  isValid() {
    const token = this.get();
    if (token) {
      const payload = this.payload(token);
      return payload.iss === AppSettings.API_ENDPOINT + "login" ? true : false;
    }
    return false;
  }
  payload(token) {
    const payload = token.split('.')[1];
    return this.decode(payload);
  }
  decode(payload) {
    return JSON.parse(atob(payload));
  }

  logOut() {
    this.remove();
    this.router.navigate(['/login']);
  }

  loggedIn() {
    return this.isValid();
  }
}
