import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';


@Injectable({
  providedIn: 'root'
})

/* This guard makes sure only authenticated users have access
to protected routes like home */

export class AuthguardService implements CanActivate  
{
  private authStatus;
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.authStatus = this.tokenService.isValid();
    if (this.authStatus) {
      return true;
    }
    else {
      this.router.navigate(['/login']);
      return false;
    }
  }
  constructor(
    private tokenService: TokenService,
    private router: Router) { }
}
