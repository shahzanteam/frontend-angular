import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './home/header/header.component';
import { SigninComponent } from './auth/signin/signin.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthguardService } from './auth/authguard.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TableCompleteComponent } from './home/table-complete/table-complete.component';
import { EdituserComponent } from './home/edituser/edituser.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home/users', pathMatch: 'full' }, //Set home/users as default route.
  { path: 'login', component: SigninComponent },
  {
    path: 'home', component: HomeComponent, canActivate: [AuthguardService], children: [
      { path: 'users', component: TableCompleteComponent },
      { path: 'edituser/:mode', component: EdituserComponent }
    ]
  },
]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SigninComponent,
    HomeComponent,
    TableCompleteComponent,
    EdituserComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
