/* Add/Edit user functionalities handled here */

import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  public form = { first_name: '', last_name: '', email: '', phone: '', mode: 'new', id: 0 };
  private response: any;
  public error: boolean = false;
  public success: boolean = false;
  public errorMessage: string;
  public successMessage: string;
  private mode: string;
  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) { }

/* Go back to users list*/
  cancel() {
    this.router.navigate(['/home/users']);
  }

  /* Add/edit user form submission*/
  onSubmit(myForm) {

    this.userService.addUser(this.form).subscribe(
      data => {
        console.log(data);
        if (data.success) {
          this.success = true;
          this.successMessage = data.message;
          myForm.reset();
          setTimeout(() => {
            this.success = false;
            this.router.navigate(['/home/users']);
          }, 2000);
        }
        else {
          this.error = true;
          this.errorMessage = data.message;
          setTimeout(() => {
            this.error = false;
          }, 3000);
        }
      },
      error => {
        this.error = true;
        this.errorMessage = "Something went wrong.Contact administrator.";
        setTimeout(() => {
          this.error = false;
        }, 3000);
      }
    );
  }
  ngOnInit() {
    this.form.mode = this.route.snapshot.params['mode'];
    var temp = this.route.snapshot.queryParams;
    if (this.form.mode === "edit") {
      this.form = {
        first_name: temp.first_name,
        last_name: temp.last_name,
        email: temp.email,
        phone: temp.phone,
        mode: 'edit',
        id: temp.id
      };
    }
  }
}


