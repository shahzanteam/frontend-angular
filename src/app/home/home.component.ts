import { Component, OnInit } from '@angular/core';
import { User } from './user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  private users: User[];
  constructor() { }
}

