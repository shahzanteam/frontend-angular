import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing'
import { HomeComponent } from './home.component';
import { TokenService } from '../auth/token.service';
import { HeaderComponent } from './header/header.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent,HeaderComponent ],
      imports: [ RouterTestingModule ] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('home component create', () => {
   let fixture=TestBed.createComponent(HomeComponent);
   let app=fixture=fixture.debugElement.componentInstance;
   expect(app).toBeTruthy();
  });

  it('home component should load only after logged in ', () => {
    let fixture=TestBed.createComponent(HomeComponent);
    let app=fixture=fixture.debugElement.componentInstance;
    let  tokenService=fixture.debugElement.injector.get(TokenService);
    expect(tokenService.isValid()).toBe(true);
   });

});
