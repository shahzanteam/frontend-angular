/* User table functionality implemented here */


import { Component, PipeTransform, } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { UserService } from '../user.service';
import { User } from '../user.model';
import { Router } from '@angular/router';

/* Users search function*/

function search(text: string, pipe: PipeTransform, users: User[]): User[] {
  const data = users;
  return data.filter(user => {
    const term = text.toLowerCase();
    return user.first_name.toLowerCase().includes(term)
      || user.last_name.toLowerCase().includes(term)
      || user.email.toLowerCase().includes(term)
      || user.phone.toLowerCase().includes(term)
  });
}

@Component({
  selector: 'app-table-complete',
  templateUrl: './table-complete.component.html',
  styleUrls: ['./table-complete.component.css'],
  providers: [DecimalPipe]
})
export class TableCompleteComponent {
  users$: Observable<User[]>;
  filter = new FormControl('');
  public users: User[];
  private sortedUsers: User[];
  private state = { first_name: false, last_name: false };
  private piper: DecimalPipe;
  constructor(
    pipe: DecimalPipe,
    private userService: UserService,
    private router: Router, ) {
    this.userService.getAllUsers().subscribe(
      (data: User[]) => {
        this.users = data;
        this.users$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text, pipe, this.users))
        );
      },
      error => {
        console.log(error);
      });
  }
  addUser() {
    this.router.navigate(['/home/edituser/new']);
  }
  /* Delete user function */

  delete(id, index) {
    this.userService.deleteUser(id).subscribe(
      data => {
        console.log(data);
        this.users.splice(index, 1);
        this.users$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text, this.piper, this.users))
        );
      }
      ,
      error => {
        alert("Something went wrong, contact administrator");
      }
    )
  }

  /* Users sort function*/

  sorter(mode: string) {
    if (this.state[mode]) {
      this.sortedUsers = this.users.sort(function (a, b) {
        return b[mode].toLowerCase().localeCompare(a[mode].toLowerCase());
      });
      this.users$ = this.filter.valueChanges.pipe(
        startWith(''),
        map(text => search(text, this.piper, this.sortedUsers))
      );
      this.state[mode] = false;
    }
    else {
      {
        this.sortedUsers = this.users.sort(function (a, b) {
          return a[mode].toLowerCase().localeCompare(b[mode].toLowerCase());
        });
        this.users$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text, this.piper, this.sortedUsers))
        );
        this.state[mode] = true;
      }
    }
  }
}



