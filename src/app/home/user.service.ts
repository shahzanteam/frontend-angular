import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../auth/token.service';
import { User } from './user.model';
import { AppSettings } from '../app-settings';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private tokenService: TokenService) { }

  getAllUsers() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + this.tokenService.get() });
    return this.http.get<User[]>(AppSettings.API_ENDPOINT + 'getAllUsers', { headers: headers })
  }

  addUser(data) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + this.tokenService.get() });
    return this.http.post<any>(AppSettings.API_ENDPOINT + 'addUser', data, { headers: headers })

  }
  deleteUser(id) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + this.tokenService.get() });
    return this.http.post<any>(AppSettings.API_ENDPOINT + 'deleteUser', { id: id }, { headers: headers })

  }
}