import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/auth/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private tokenService: TokenService) { }

  logout() {
    this.tokenService.logOut();
  }
}
